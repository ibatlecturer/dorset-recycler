package com.example.recyclerdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.demo_row.view.*

class MainAdapter(val homeFeed: HomeFeed) : RecyclerView.Adapter<CustomViewHolder>() {


    val listOfItemTitles = listOf<String>("First","Second", "Third")

   override  fun getItemCount() : Int {
      //  return 3

        //return listOfItemTitles.size

       return homeFeed.videos.count();
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val layoutInflator = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflator.inflate(R.layout.demo_row, parent, false)

        return CustomViewHolder(cellForRow)
    }
//This is how we rebind

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.


       // val itemTitle = listOfItemTitles.get(position)
        val itemTitle = homeFeed.videos.get(position)

        holder?.itemView?.textViewDemoTitle.text = itemTitle.name


    }
}

class CustomViewHolder (view: View) : RecyclerView.ViewHolder(view) {

}
package com.example.recyclerdemo

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Step 1: Change the Background Colour - then commnet out once you see it working
        //   my_recycler_view.setBackgroundColor(Color.BLUE)

        //Step 2: Set up a layout manager
        my_recycler_view.layoutManager = LinearLayoutManager(this)

        //Step 3: SEt up an adapter -- create this in a new class then come back here
        //my_recycler_view.adapter = MainAdapter()

        //Step 4: Remove above line for http part of demo

        fetchJson()

    }


    fun fetchJson() {

        Log.i("HTTPDEMO", "Loading JSON")

        //  var url = "https://my-json-server.typicode.com/johnrowley/demojson/posts"

        var url = "https://api.letsbuildthatapp.com/youtube/home_feed"

        // Request object
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {


                println("Failed to load JSON")
            }

            override fun onResponse(call: Call, response: Response) {

                val body = response?.body?.string()
                println(body)
                Log.i("JSON", body)

                val gson = GsonBuilder().create()
                val homeFeed = gson.fromJson(body, HomeFeed::class.java)
                //Sample Output (assumes we know we got everythung correctly)
                Log.i("JSON", homeFeed.videos[0].name)
                runOnUiThread {
                    my_recycler_view.adapter = MainAdapter(homeFeed)
                }
            }


        })


    }
}


class HomeFeed(val videos: List<Video>)
//You would map out all ids here
class Video(val id: Int, val name: String)